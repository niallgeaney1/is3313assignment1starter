<%-- 
    Document   : calc
    Created on : 2 Dec 2020, 14:21:37
    Author     : simon
--%>

<%@page import="com.simonwoodworth.javamavencalcinclassdemo.Calculator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello IS3313!</h1>
        <p>This is to demonstrate changes after the first commit and push.</p>
        <p><%= new java.util.Date() %></p>
        <p><%= new Calculator().add(7, 5) %></p>
        <p><%= new Calculator().subtract(7, 5) %></p>
        <p><%= new Calculator().multiply(7, 5) %></p>
        <p><%= new Calculator().divide(7, 5) %></p>
        <h2>Your code demonstrating correct operation of square, cube and module goes below</h2>
        <p><%= new Calculator().square(8) %></p>
        <p><%= new Calculator().cube(8) %></p>
        <p><%= new Calculator().modulo(8, 5) %></p>
    </body>
</html>
